---
title: Quick Typer
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

 - **Self destructing messages.**
[Syntax: `quicktype <x> <message>`]
  
   Creates a self destructing `<message>` which gets deleted after `<x>` seconds.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE2NDAxNTU2MTVdfQ==
-->
