---
title: Installation
layout: default
nav_order: 1
has_children: true
---

# {{ page.title }}

There are several methods to install. I recommend Docker if you use desktop Linux, or otherwise Automated.
